import * as React from "react";
import { Container, ContainerSize, IButtonBarItem } from "./container";
import { LoremIpsum } from "lorem-ipsum"

interface IProps {
}

interface IState {
    text: string;
}

export class LoremIpsumGenerator extends React.Component<IProps, IState> {

    input: HTMLTextAreaElement;
    loremIpsum = new LoremIpsum({
        sentencesPerParagraph: 
        {
          max: 8,
          min: 4
        },
        wordsPerSentence: {
          max: 16,
          min: 4
        }
      });

    constructor(props: IProps){
        super(props);

        this.state = {
            text: this.loremIpsum.generateParagraphs(1)
        }
    }

    copy = () => {
        this.input.select();
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
        this.input.blur();
    }

    createNew = () => {
        this.setState({
            text: this.loremIpsum.generateParagraphs(1)
        });
    }

    renderbody(): JSX.Element{
        return <textarea
            id="LoremIpsumContainer" 
            readOnly={true} 
            value={this.state.text}
            ref={(input) => this.input = input}
        />;
    }

    renderButtons(): IButtonBarItem[]{
        return [
            {
                key: "copy",
                button: <button onClick={this.copy} className="">Copy</button>
            },
            {
                key: "create",
                button: <button onClick={this.createNew} className="raised">Give me a new one!</button>
            }
        ];
    }

    render() {
        return <Container size={ContainerSize.large} title="Lorem Ipsum Generator" body={this.renderbody()} buttonBar={this.renderButtons()} />;        
    }
}