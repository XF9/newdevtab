import * as React from "react";

export enum ContainerSize{
    regular,
    large
}

interface IProps {
    title: string;
    body: JSX.Element;
    size?: ContainerSize;
    buttonBar?: IButtonBarItem[];
}

export interface IButtonBarItem{
    key: string;
    button: JSX.Element
}

function ContainerSizeToClassName(size: ContainerSize): string{
    if(size === ContainerSize.large)
        return "large";
    return "";
}

export const Container = (props: IProps) => {
    return <section className={"container " + ContainerSizeToClassName(props.size)}>
        <header>
            <h2>
                {props.title}
            </h2>
        </header>
        <article>{props.body}</article>
        {
            props.buttonBar !== null &&
            <footer>
                {
                    props.buttonBar.map((item => <span key={item.key}>{item.button}</span>))
                }
            </footer>
        }
    </section>;
}