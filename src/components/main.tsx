import "../style/main.less";

import * as React from "react";
import { Headline } from "./headline";
import { GuidGenerator } from "./guidGenerator";
import { LoremIpsumGenerator } from "./loremIpsumGenerator";

export class Main extends React.Component<{}, {}> {

    ch: string;
    cb: JSX.Element;

    constructor(props: {}){
        super(props);
        this.ch = "Head";
        this.cb = <div>Body</div>;
    }

    render() {
        return <div>
            <Headline name="Michael"/>
            <div id="ContainerBox">
                <GuidGenerator />
                <LoremIpsumGenerator />
            </div>
        </div>;
    }
}