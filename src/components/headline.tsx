import * as React from "react";
import * as dateFormat from "dateformat"

interface IProps {
    name: string;
}

interface IState {
    date: Date;
}

export class Headline extends React.Component<IProps, IState> {

    timerID: number;

    constructor(props: IProps){
        super(props);

        this.state = {
            date: new Date()
        }
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
      }
    
      componentWillUnmount() {
        clearInterval(this.timerID);
      }
    
      tick() {
        this.setState({
          date: new Date()
        });
      }

    render() {
        return <header>
            <h1>Good morning {this.props.name}!</h1>
            <div>
              Today is 
              <span className="highlight">{dateFormat(this.state.date, " dddd, mmmm dS ")}</span>
              and it's 
              <span className="highlight">{dateFormat(this.state.date, " HH:MM")}</span>
              .
            </div>
        </header>;        
    }
}