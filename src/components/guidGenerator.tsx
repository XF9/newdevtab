import * as React from "react";
import { v4 } from "uuid"
import { Container, IButtonBarItem } from "./container";

interface IProps {
}

interface IState {
    guid: string;
}

export class GuidGenerator extends React.Component<IProps, IState> {

    input: HTMLInputElement;

    constructor(props: IProps){
        super(props);

        this.state = {
            guid: v4().toUpperCase()
        }
    }

    copy = () => {
        this.input.select();
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
        this.input.blur();
    }

    createNew = () => {
        this.setState({
            guid: v4().toUpperCase()
        });
    }

    renderbody(): JSX.Element{
        return <input 
            type="text" 
            id="Guid-Container" 
            readOnly={true} 
            value={this.state.guid}
            ref={(input) => this.input = input}
        ></input>;
    }

    renderButtons(): IButtonBarItem[]{
        return [
            {
                key: "copy",
                button: <button onClick={this.copy} className="">Copy</button>
            },
            {
                key: "create",
                button: <button onClick={this.createNew} className="raised">Give me a new one!</button>
            }
        ];
    }

    render() {
        return <Container title="Guid Generator" body={this.renderbody()} buttonBar={this.renderButtons()} />;
    }
}